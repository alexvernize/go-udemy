module modulo

go 1.18

//Adicionando pacotes externos, vá até a raiz do projeto e rode "go get" com o dimínio do pacote desejado
//Rodando o seguinte comando temos o resultado abaixo "go get github.com/badoux/checkmail"
require github.com/badoux/checkmail v1.2.1 // indirect

//****IMPORTANTE: Não mexa ou faça modificações manuais, tente sempre utilizar o "go get"
//Para remover as dependências/pacotes que não estão sendo usados, rode o comando "go mod tidy"
//e ele irá remover oq não estiver em uso
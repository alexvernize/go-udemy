package main

import (
	"fmt"
	"modulo/auxiliar"

	"github.com/badoux/checkmail"
)

func main() {
	fmt.Println("Escrevendo do arquivo main")
	//É utilizado maiúsculas para se referir a funções que são globais, com letra minúscula é
	//utilizado para funções dentro da mesma classe/função/arquivo/pacote
	auxiliar.Escrever()

	//Para chamar um módulo utilizamos sempre o que aparece depois da última barra '/', por exemplo
	//o pacote externo github.com/badoux/checkmail só irei colocar o que estiver depois da última barra
	erro := checkmail.ValidateFormat("devbook@gmail.com")
	fmt.Println(erro)

}

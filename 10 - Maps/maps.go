//Maps são estrutura para guardar dados, assim como arrays e slices, elas utilizam
//o formato de key:value e esses tipos de dados de ambos são passados na estrutura

package main

import "fmt"

func main() {
	fmt.Println("Maps")

	//Criando um map, dentro do colchetes é o tipo das chaves e fora do colchetes
	//é o tipo dos valores. Não esqueça das vírgulas depois de cada valor
	usuario := map[string]string{
		"nome":      "Pedro",
		"sobrenome": "Silva",
	}

	fmt.Println(usuario)

	//Para acessar o valor de um map, utilize o formato abaixo
	fmt.Println(usuario["nome"])

	//Exemplo de uso do map com a key como int
	usuario2 := map[int]string{
		1: "Pedro",
		2: "Silva",
	}

	fmt.Println(usuario2)
	fmt.Println(usuario2[2])

	//É possível utilizar o map aninhado
	usuario3 := map[string]map[string]string{
		"nome": {
			"primeiro": "João",
			"último":   "Pedro",
		},
		"curso": {
			"nome":   "Engenharia",
			"campus": "Campus 1",
		},

		//Isso abaixo não funciona, porque ele sempre recebe um tipo map
		//"materia": "fisica",
	}

	fmt.Println(usuario3)

	//É possível deletar uma das chaves com uma função nativa do go chamada delete
	delete(usuario, "nome")
	fmt.Println(usuario)

	//Abaixo um exemplo de adição de uma chave
	usuario3["signo"] = map[string]string{
		"nome": "Gêmeos",
	}
	fmt.Println(usuario3)
}

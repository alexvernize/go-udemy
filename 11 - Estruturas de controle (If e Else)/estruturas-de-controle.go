/*Condicional em go é bem parecido com as demais linguagens.
* Não é necessário adicionar parênteses depois do if na
* condição que estiver colocando.
 */

package main

import "fmt"

func main() {
	fmt.Println("Estruturas de controle")

	numero := -5

	if numero > 15 {
		fmt.Println("Maior que 15")
	} else {
		fmt.Println("Menor ou igual a 15")
	}

	//É possível criar a variável e já colocar a condição na mesma linha.
	//É chamado de if init
	if outroNumero := numero; outroNumero > 0 {
		fmt.Println("Número é maior que zero")
	} else if numero < -10 {
		fmt.Println("Número é menor que -10")
	} else {
		fmt.Println("Entre 0 e -10")
	}

	//Criando uma variável dessa forma, a limitação é que ela só funciona
	//e aparece no escopo do if/else e não pode ser usada no mesmo coódigo
	//em outra parte. Por exemplo abaixo tentamos utilizar ela fora do if/else
	//o go irá apresentar um erro se tentarmos.
	//fmt.Println(outroNumero)
}

//Não é necessário adicionar a palavra break no switch, o go automaticamente sai
//da claúsula selecionada.

package main

import "fmt"

func diaDaSemana(numero int) string {
	//Existem duas formas de criar um switch, uma forma é passar a variável após
	//a palavra switch, igual o primeiro exemplo abaixo, a outra forma é fazer a
	//comparação caso a caso, igual o segundo exemplo abaixo.
	switch numero {
	case 1:
		return "Domingo"
	case 2:
		return "Segunda-Feira"
	case 3:
		return "Terça-Feira"
	case 4:
		return "Quarta-Feira"
	case 5:
		return "Quinta-Feira"
	case 6:
		return "Sexta-Feira"
	case 7:
		return "Domingo"
	default:
		return "Número Inválido"
	}
}

func diaDaSemana2(numero int) string {
	//Também é possível passar uma variável para fazer a comparação
	//com o valor do switch, sem adicionar um return para cada caso
	var diaDaSemana string

	switch {
	//Para switch temos o fallthrough que joga para a claúsula seguinte o retorno
	//por exemplo com o número 1 o retorno seria Domingo, porém com o fallthrough
	//ele passaria para o próximo e retornaria segunda
	case numero == 1:
		diaDaSemana = "Domingo"
		//fallthrough
	case numero == 2:
		diaDaSemana = "Segunda-Feira"
	case numero == 3:
		diaDaSemana = "Terça-Feira"
	case numero == 4:
		diaDaSemana = "Quarta-Feira"
	case numero == 5:
		diaDaSemana = "Quinta-Feira"
	case numero == 6:
		diaDaSemana = "Sexta-Feira"
	case numero == 7:
		diaDaSemana = "Domingo"
	default:
		diaDaSemana = "Número Inválido"
	}

	return diaDaSemana
}

func main() {
	dia := diaDaSemana(200)
	fmt.Println(dia)

	fmt.Println("-----------")
	dia2 := diaDaSemana2(1)
	fmt.Println(dia2)
}

//Em go só temos o loop for
//Não é possível usar o range em structs

package main

// import (
// 	"fmt"
// 	"time"
// )
func main() {
	// i := 0

	// for i < 10 {
	// 	i++
	// 	fmt.Println("Incrementando i")
	// 	time.Sleep(time.Second)
	// }

	// fmt.Println(i)

	//Não esquecer que ao passar e iniciar uma variável, ela só fica visível dentro
	//do laço for
	// for j := 0; j < 10; j += 5 {
	// 	fmt.Println("Incrementando j", j)
	// 	time.Sleep(time.Second)
	// }

	////O range é usado quando queremos iterar um array, slice, etc
	//nomes := [3]string{"João", "Davi", "Lucas"}
	////No laço o primeiro valor passado é o índice "indice" do array, o segundo é o
	////valor do array "nome", o mesmo do exemplo abaixo. Algo como nomes[0].
	// for indice, nome := range nomes {
	// 	fmt.Println(indice, nome)
	// }

	////Se você não quiser utilizar o índice só utilizar o "_" no lugar.
	// for _, nome := range nomes {
	// 	fmt.Println(nome)
	// }

	////Podemos iterar com strings, mas o retorno será do valor ASCII de cada caracter
	// for indice, letra := range "PALAVRA" {
	// 	fmt.Println(indice, letra)

	////Caso queira a letra mesmo propriamente dita, utilize a função string()
	// 	fmt.Println(indice, string(letra))
	// }

	// //Iteração com map
	// usuario := map[string]string{
	// 	"nome":      "Leonardo",
	// 	"sobrenome": "Silva",
	// }

	// for chave, valor := range usuario {
	// 	fmt.Println(chave, valor)
	// }

	// for _, valor := range usuario {
	// 	fmt.Println(valor)
	// }

	////Para criar um loop infinito é só usar for{}
	// for {
	// 	fmt.Println("Executando infinitamente")
	// 	time.Sleep(time.Second)
	// }
}

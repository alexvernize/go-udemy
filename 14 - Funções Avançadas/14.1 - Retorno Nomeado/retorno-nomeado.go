package main

import "fmt"

//Função com retorno nomeado, tem um nome para as variáveis que serão retornadas.
//O exemplo abaixo tem uma variável "soma" e uma "subtracao" sendo retornadas.
func calculosMatematicos(n1, n2 int) (soma int, subtracao int) {
	//Como essa função já tem o nome e já foi "inicializada" não é necessário
	//inicializar ela com o :=, podemos usar apenas o = como abaixo.
	soma = n1 + n2
	subtracao = n1 - n2
	return
}

func main() {
	soma, subtracao := calculosMatematicos(10, 20)
	fmt.Println(soma, subtracao)
}

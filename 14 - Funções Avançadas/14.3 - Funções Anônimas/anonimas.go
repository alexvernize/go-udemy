package main

import "fmt"

func main() {
	//Pra utilizar uma função anônima criamos a função e no final dela abrimos
	//e fechamos parênteses e ela será executada.
	func() {
		fmt.Println("Olá mundo")
	}()

	//É possível passar parâmetros para as funções anônimas da seguinte forma
	func(texto string) {
		fmt.Println(texto)
	}("Passando um parâmetro diretamente")

	//Aqui utilizamos uma variável retorno para receber o retorno e depois fazemos o print.
	//A função Sprintf concatena parâmetros.
	retorno := func(texto string) string {
		return fmt.Sprintf("Recebido -> %s", texto)
	}("Passando Parâmetro")

	fmt.Println(retorno)
}

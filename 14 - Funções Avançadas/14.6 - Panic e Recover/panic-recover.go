package main

import "fmt"

func recuperarExecucao() {
	//Recupera a execução de um programa que entra na função panic
	if r := recover(); r != nil {
		fmt.Println("Execução recuperada com sucesso!")
	}
}

func alunoEstaAprovado(n1, n2 float64) bool {
	//Quando o panic vai ser executado ele chama antes as funções com defer
	defer recuperarExecucao()
	media := (n1 + n2) / 2

	if media > 6 {
		return true
	} else if media < 6 {
		return false
	}

	//a função panic mata a execução do programa, se você não tiver um recover ela vai matar
	//todas as execuções
	panic("A MÉDIA É EXATAMENTE 6!")
}

func main() {
	fmt.Println(alunoEstaAprovado(8, 6))
	fmt.Println("Pós execução!")
}

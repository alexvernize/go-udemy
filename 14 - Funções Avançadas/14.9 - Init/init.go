package main

import "fmt"

var n int

//Serve para ser executada antes da função main. Pode ter uma função init por arquivo, e
//não uma por pacote.
func init() {
	fmt.Println("Executando a função init")
	//Pode ser usada para inicializar uma variável
	n = 10
}

func main() {
	fmt.Println("Função main sendo executada")
	fmt.Println(n)
}

package main

//O go é uma linguagem fortemente tipada, isso quer dizer que todas
//as variáveis obrigatoriamente vão ter um tipo definido
//O go não deixa você criar uma variável e não utilizar
//o mesmo vale para pacotes
import "fmt"

func main() {
	//Declarando uma variável com o tipo explícito
	var variavel string = "Variavel 1"

	//Declarando uma variável com o tipo implícito
	variavel2 := "Variavel 2"
	fmt.Println(variavel)
	fmt.Println(variavel2)

	//Declarando múltiplas variáveis
	var (
		variavel3 string = "variavel 3"
		variavel4 string = "variavel 4"
	)

	fmt.Println(variavel3, variavel4)

	//Outra forma de declarar múltiplas variáveis
	variavel5, variavel6 := "variavel 5", "variavel 6"

	fmt.Println(variavel5, variavel6)

	//Constantes em go
	const constante1 string = "constante 1"
	fmt.Println(constante1)

	//Invertendo valores de variáveis
	variavel5, variavel6 = variavel6, variavel5
	fmt.Println(variavel5, variavel6)

}

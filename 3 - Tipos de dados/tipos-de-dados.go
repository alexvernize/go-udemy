/*Tipos de dados inteiros int8, int16, int32, int64 (O número depois de int é a quantidade de
bits que ele suporta) se você não especificar ele pega a arquitetura do computador 32 ou 64 bits

Tipos de dados float32 e float64

Tipo de dado string - IMPORTANTE em go sempre utilizar aspas duplas ""
aspas simples '' podem ser utilizadas para caracteres e pegar o número dele na tabela ASCII

Tipo de dado boleano "false" ou "true"

Tipo de dado erro "error"
*/

package main

import (
	"errors"
	"fmt"
)

func main() {

	var numero int16 = 100
	fmt.Println(numero)

	//unit é o número sem sinal também é possívem utilizar com os bits ex: uint8, uint16, ...
	var numero2 uint = 1000 //Se eu utilizar um sinal no número ele irá dar erro pq é um uint
	fmt.Println(numero2)

	// alias para as variaveis
	var numero3 int32 = 123544
	//O rune é o alias do int32 é exatamente a mesma coisa mas com o nome diferente
	//int32 = rune
	var numero4 rune = 123545
	// O alias do uint8 é o byte
	//byte = uint8
	var numero5 byte = 32
	fmt.Println(numero3)
	fmt.Println(numero4)
	fmt.Println(numero5)

	var numeroReal1 float32 = 12121245.21245454
	fmt.Println(numeroReal1)
	var numeroReal2 float64 = 12121245.21245454
	fmt.Println(numeroReal2)
	numeroReal3 := 21212.21212
	fmt.Println(numeroReal3)

	var str string = "Texto"
	fmt.Println(str)

	char := 'A'
	fmt.Println(char)

	var booleano bool = true
	fmt.Println(booleano)

	//Para usar o tipo erro, utilizamos o pacote errors
	var erro error = errors.New("Erro interno")
	fmt.Println(erro)

	//Se não for atribuído um valor para a variável, no go ela sempre inicia com um valor default
	//para strings é vazio e número o valor 0, para o booleano é false e para erro o valor é <nil>
	var texto2 string
	fmt.Println(texto2)
	var numero6 int
	fmt.Println(numero6)
	var boole bool
	fmt.Println(boole)
	var erro2 error
	fmt.Println(erro2)
}

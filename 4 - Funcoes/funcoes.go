package main

import "fmt"

//Função é um conjunto de instruções que rodam comanados
//Para criar uma função utilizamos a palavra "func" seguida
//de um nome pra função. Entre os parênteses passamos os parâmetros.
//Se a função tiver um retorno, o tipo deve ser passado após o parênteses
// por exemplo: func teste (numero1 int16, numero2 int8) int16 {}
func somar(n1 int8, n2 int8) int8 {
	return n1 + n2
}

//As funções podem ter mais de um retorno nesse exemplo abaixo ela
//vai trazer dois retornos do tipo int8
func calculosMatematicos(n1, n2 int8) (int8, int8) {
	soma := n1 + n2
	subtracao := n1 - n2
	return soma, subtracao
}

func main() {
	soma := somar(10, 20)
	fmt.Println(soma)

	//Funções aninhadas (utilizando uma função como retorno)
	var f = func() {
		fmt.Println("Função f")
	}

	//Chamando uma função
	f()

	//Passando parâmetros para a função
	var f2 = func(txt string) {
		fmt.Println(txt)
	}

	f2("Passando um texto para a funcão")

	//Função com retorno do tipo string de exemplo
	var f3 = func(txt string) string {
		fmt.Println(txt)
		return txt
	}

	resultado := f3("Texto de retorno")
	fmt.Println(resultado)

	//Para utilizar uma função com dois retornos
	resultadoSoma, resultadoSubtracao := calculosMatematicos(10, 15)
	fmt.Println(resultadoSoma, resultadoSubtracao)

	//Para não precisar usar uma variável em go que foi declarada, utilize o "_"
	resultadoSoma2, _ := calculosMatematicos(10, 15)
	fmt.Println(resultadoSoma2)

	_, resultadoSubtracao2 := calculosMatematicos(10, 15)
	fmt.Println(resultadoSubtracao2)
}

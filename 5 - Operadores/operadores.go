package main

import "fmt"

func main() {
	//ARITMÉTICOS +, -, /, *, %
	soma := 1 + 2
	var subtracao int8 = 1 - 2
	divisao := 10 / 4
	var multiplicacao int16 = 10 * 5
	var restoDaDivisao float32 = 10 % 2

	fmt.Println("Soma: ", soma, "\nSubtracao: ", subtracao, "\nDivisao: ", divisao, "\nMultiplicacao: ", multiplicacao, "\nResto da divisão: ", restoDaDivisao)

	//Em go não é possível realizar operações entre valores com tipos diferentes
	// somar um int16 com um int32
	var numero1 int16 = 30
	var numero2 int32 = 25
	//Para realizar essa operação é necessário deixar ambos com o mesmo tipo
	//como abaixo que o int16 é colocado no numero2
	soma2 := numero1 + int16(numero2)
	fmt.Println(soma2)
	//FIM DOS ARITMÉTICOS

	//ATRIBUIÇAO =, :=
	var variavel1 string = "String"
	variavel2 := "String"
	fmt.Println(variavel1, variavel2)
	//FIM DOS OPERADORES DE ATRIBUIÇÃO

	//OPERADORES RELACIONAIS >, >=, <, <=, == e != (Sempre retorna "true" ou "false")
	fmt.Println(1 > 2)  //maior
	fmt.Println(1 >= 2) //maior ou igual
	fmt.Println(1 < 2)  //menor
	fmt.Println(1 <= 2) //menor ou igual
	fmt.Println(1 == 2) //igualdade
	fmt.Println(1 != 2) //diferente
	//FIM DOS RELACIONAIS

	//OPERADORES LÓGICOS &&, || e !
	verdadeiro, falso := true, false
	fmt.Println(verdadeiro && falso) // e
	fmt.Println(verdadeiro || falso) // ou
	fmt.Println(!verdadeiro)         // negação (not)
	fmt.Println(!falso)              // negação (not)
	//FIM DOS OPERADORES LÓGICOS

	//OPERADORES UNÁRIOS ++, --, +=, -=, *=, /= ...
	numero := 10
	numero++     // é o mesmo que fazer numero = numero + 1
	numero += 20 // é o mesmo que fazer numero = numero + 15
	fmt.Println(numero)
	//FIM DOS OPERADORES UNÁRIOS

}

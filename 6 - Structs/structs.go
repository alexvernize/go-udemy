/*Struct ~e uma coleção de campos, cada campo tem um nome e um tipo
 */

package main

import "fmt"

//Criando um struct de usuário com dois campos
type usuario struct {
	nome     string
	idade    uint8
	endereco endereco
}

//Criando um struct e usando como argumento na struct acima
type endereco struct {
	logradouro string
	numero     uint16
}

func main() {
	fmt.Println("Arquivo structs")

	//Existem duas formas de criar uma struct
	//Criando struct forma 1
	var u usuario
	u.nome = "Alex"
	u.idade = 32
	fmt.Println(u)
	fmt.Println(u.nome)
	fmt.Println(u.idade)

	enderecoExemplo := endereco{"Frederico Maurer", 2538}

	//Criando struct forma 2
	usuario2 := usuario{"Alex", 32, enderecoExemplo}
	fmt.Println(usuario2)

	//Caso queira passar apenas um valor
	usuario3 := usuario{nome: "Alex"}
	fmt.Println(usuario3)

	//Quando você não coloca valor ele passa o campo com o valor 0
	var u2 usuario
	fmt.Println(u2)

}

package main

import "fmt"

//Observação: O go deixa você criar struct e não usar, diferente
//das variáveis
type pessoa struct {
	nome      string
	sobrenome string
	idade     uint8
	altura    uint8
}

//Um estudante tamb~em é uma pessoa, então só passamos a struct
//pessoa sem nenhum tipo (int, string, ...)
type estudante struct {
	pessoa    //Esse é o único caso que não precisa especificar o tipo
	curso     string
	faculdade string
}

func main() {
	fmt.Println("Herança")

	p1 := pessoa{"João", "Pedro", 20, 178}
	fmt.Println(p1)

	estudante1 := estudante{p1, "Engenharia", "USP"}
	fmt.Println(estudante1)
	fmt.Println(estudante1.nome)
}

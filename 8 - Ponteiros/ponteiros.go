package main

import "fmt"

func main() {
	fmt.Println("Ponteiros")

	/* Quando copiamos uma variável, copiamos apenas o valor dela.
	Se trabalharmos com ela depois, o valor inicial que será
	copiado. Quando utilizamos ponteiros, colocamos a posição dela
	na memória, fazendo uma REFERÊNCIA DE MEMÓRIA, então qualquer
	alteração será passada para a varável que for copiada */
	var variavel1 int = 10
	var variavel2 int = variavel1

	variavel1++
	fmt.Println(variavel1, variavel2)

	//Para criar um ponteiro colocamos o símbolo de "*" na frente da variável
	//Para atribuir uma variável para um ponteiro utilizamos o "&"
	var variavel3 int
	var ponteiro *int

	variavel3 = 100
	//Eu não jogo o valor 100 na variável ponteiro, eu jogo o endereço de memória
	//que ele está
	ponteiro = &variavel3
	fmt.Println(variavel3, ponteiro)
	//Para ver o valor que esse endereço de memória possui, utilizamos o "*" na
	//frente da variável, como abaixo
	fmt.Println(variavel3, *ponteiro)
}

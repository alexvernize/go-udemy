package main

import (
	"fmt"
	"reflect"
)

func main() {
	fmt.Println("Arrays e Slices")
	//CRIANDO UM ARRAY
	//Para criar um array utilizamos um nome seguido do tamanho entre colchetes [n]
	// e depois o tipo que esse array vai receber de dados (int, float, string, ...)
	var array [5]string
	array[0] = "Posição 1"
	array[3] = "Posição 4"
	fmt.Println(array)

	array2 := [5]string{"Posição 1", "Posição 2"}
	fmt.Println(array2)

	//É possível utilizar três pontos dentro do colchetes "..." e popular depois com valores
	//o go vai calcular a quantidade de valores nesse array. IMPORTANTE: Esse formato não deixa
	//ele com tamanho dinâmico, apenas deixa ele com o tamanho baseado na quantidade de valores
	//colocados.
	array3 := [...]int{1, 2, 3, 4, 5, 6}
	fmt.Println(array3)

	//CRIANDO SLICE
	//O slice é mais flexível que um array e mais utilizado em go, ele tem um tamanho dinâmico.
	//Ele é parecido com um array, mas não é um array.
	slice := []int{10, 11, 12, 15, 17} //Utilizado nome slice só de exemplo, não é uma palavra reservada
	fmt.Println(slice)

	//Essa função devolve o tipo da variável
	//Exemplo para mostrar que array é diferente de slice
	fmt.Println(reflect.TypeOf(array3))
	fmt.Println(reflect.TypeOf(slice))

	//Função que adiciona valores no slice. Ela pega o slice, adiciona o valor e devolve um slice
	//com novos valores
	slice = append(slice, 18)
	fmt.Println(slice)

	//É possível pegar um array e passar para um slice (ou uma parte dele)
	slice2 := array2[1:3] //Pegando os valores das posições 1,2 e 3 do array2
	fmt.Println(slice2)

	array2[2] = "Posição alterada" //Ele funciona como um ponteiro que pega o valor e altera pela posição
	fmt.Println(slice2)

	//ARRAYS INTERNOS
	fmt.Println("-----------------------")
	slice3 := make([]float32, 10, 11)
	fmt.Println(slice3)
	fmt.Println(len(slice3)) //Função lenght, para saber o tamanho do array/slice
	fmt.Println(cap(slice3)) //Função cap, para saber a capacidade do array/slice

	//Sempre que um slice tem o tamanho estourado ele pega a capacidade máxima e dobra o valor acima temos o
	//slice com 10 elementos e tamanho máximo de 11, abaixo adicionamos 2 novos elementos o que acaba estourando
	//o valor máximo de 11 e indo para 12, o go pega os elementos e dobra o valor, de 12 para 24
	slice3 = append(slice3, 5)
	slice3 = append(slice3, 6)
	fmt.Println(slice3)
	fmt.Println(len(slice3)) //Função lenght, para saber o tamanho do array/slice
	fmt.Println(cap(slice3)) //Função cap, para saber a capacidade do array/slice

	//Não é necessário referenciar o valor máximo do slice
	slice4 := make([]float32, 5)
	fmt.Println(slice4)
	fmt.Println(len(slice4))
	fmt.Println(cap(slice4))

	slice4 = append(slice4, 10)
	fmt.Println(slice4)
	fmt.Println(len(slice4))
	fmt.Println(cap(slice4))

}
